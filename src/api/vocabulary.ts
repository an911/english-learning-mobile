import { request } from '../config/axios';

export const getVocabularyPackages = (data: {
	root?: boolean;
	onlyChild?: boolean;
	parent?: string;
	page?: number;
	limit?: number;
}) => {
	const endpoint = `/vocabularyPackages?root=${data.root}&parent=${data.parent}&page=${data.page}&limit=${data.limit}&onlyChild=${data.onlyChild}`;
	return request(endpoint, 'GET', null);
};
