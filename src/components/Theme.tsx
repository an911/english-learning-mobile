import {createBox, createText, createTheme} from '@shopify/restyle';

const palette = {
  featherGreen: '#58cc02',
  turtle: '#a5ed6e',
  seaSponge: '#d7ffb8',
  macaw: '#1cb0f6',
  iguana: '#ddf4ff',
  grayDark: '#3c3c3c',
  tundora: '#4c4c4c',
  muted: '#afafaf',
  alto: '#cfcfcf',
  doveGray: '#6f6f6f',
  persianRed: '#d33131',
  cinnabar: '#e53838',
  flamingo: '#ffb2b2',
  walkingFish: '#ffdfe0',

  borderColorDefault: '#e5e5e5',
  borderColorPrimary: '#58a700',
  borderColorSecondary: '#1899d6',

  black: '#0B0B0B',
  white: '#FFFFFF',
  indicator: 'rgba(0,0,0,0.3)',
  transparent: 'transparent',
};

const theme = createTheme({
  colors: {
    transparent: palette.transparent,
    primary: palette.featherGreen,
    secondary: palette.macaw,
    white: palette.white,
    text: palette.grayDark,
    persianRed: palette.persianRed,
    cinnabar: palette.cinnabar,
    black: palette.black,
    borderColorDefault: palette.borderColorDefault,
    borderColorPrimary: palette.borderColorPrimary,
    borderColorSecondary: palette.borderColorSecondary,
    muted: palette.muted,
    doveGray: palette.doveGray,
    alto: palette.alto,
    turtle: palette.turtle,
    seaSponge: palette.seaSponge,
    flamingo: palette.flamingo,
    walkingFish: palette.walkingFish,
    iguana: palette.iguana,
  },
  textVariants: {
    header: {
      fontSize: 32,
      lineHeight: 40,
      fontFamily: 'DinRoundPro-Bold',
    },
    title: {
      fontSize: 24,
      fontFamily: 'DinRoundPro-Medi',
      fontWeight: 'bold',
      color: 'text',
    },
    title2: {
      fontSize: 20,
      fontFamily: 'DinRoundPro-Bold',
      color: 'muted',
    },
    subTitle: {
      fontSize: 17,
      lineHeight: 19,
      fontFamily: 'DinRoundPro-Medi',
      color: 'doveGray',
    },
    label: {
      fontSize: 19,
      fontFamily: 'DinRoundPro-Bold',
      color: 'doveGray',
    },
    body: {
      fontSize: 19,
      fontFamily: 'DinRoundPro-Medi',
      color: 'text',
    },
  },
  borderRadius: {
    n: 0,
    xs: 4,
    s: 10,
    m: 16,
    l: 24,
    xl: 75,
  },
  spacing: {
    n: 0,
    xs: 4,
    s: 8,
    m: 16,
    l: 24,
    xl: 40,
  },
  breakpoints: {
    phone: 0,
    tablet: 768,
  },
});

export type Theme = typeof theme;
export const Text = createText<Theme>();
export const Box = createBox<Theme>();

export default theme;
