export const name = 'Auth';
export interface IAuthenticationState {
	accessToken: string;
	refreshToken: string;
	isAdmin: boolean;
	isProcessing: boolean;
}

// InitialState
export const initialState: IAuthenticationState = {
	accessToken: '',
	refreshToken: '',
	isAdmin: false,
	isProcessing: false,
};
