import {IError} from './../../../common/constants';
/**
 * @file All actions interface will be listed here
 */

import {Action} from 'redux';
import Keys from './actionKeys';

export interface IHandleClear extends Action {
  readonly type: Keys.HANDLE_CLEAR;
  payload: {
    type: 'all';
  };
}

export interface IGetStoredAuthentication extends Action {
  readonly type: Keys.GET_STORED_AUTHENTICATION;
  payload: {
    accessToken: any;
  };
}

//#region User Login IActions
export interface IUserLogin extends Action {
  readonly type: Keys.USER_LOGIN;
  payload: {
    email: string;
    password: string;
  };
}

export interface IUserLoginSuccess extends Action {
  readonly type: Keys.USER_LOGIN_SUCCESS;
  payload: {
    accessToken: string;
    refreshToken: string;
    isAdmin: boolean;
  };
}

export interface IUserLoginFail extends Action {
  readonly type: Keys.USER_LOGIN_FAIL;
  payload: IError[];
}
//#endregion

//#region User Register IActions
export interface IUserRegister extends Action {
  readonly type: Keys.USER_REGISTER;
  payload: {
    email: string;
    password: string;
  };
}

export interface IUserRegisterSuccess extends Action {
  readonly type: Keys.USER_REGISTER_SUCCESS;
  payload: any;
}

export interface IUserRegisterFail extends Action {
  readonly type: Keys.USER_REGISTER_FAIL;
  payload: IError[];
}
//#endregion
