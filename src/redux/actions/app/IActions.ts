/**
 * @file All actions interface will be listed here
 */

import {Action} from 'redux';
import Keys from './actionKeys';
import {
  ILesson,
  ILessonRecord,
  IPagination,
  IUserHistory,
  IUserInfo,
  IVocabulary,
  IVocabularyPackage,
} from './../../model/IApp';
import { ImagePickerResult } from 'expo-image-picker';

export interface IHandleCurrentLesson extends Action {
  readonly type: Keys.HANDLE_CURRENT_LESSON;
  payload: {
    type: 'detail';
    lesson: ILesson;
  };
}

export interface IHandleUserAnswer extends Action {
  readonly type: Keys.HANDLE_USER_ANSWER;
  payload: any;
}

export interface IUserLogOut extends Action {
  readonly type: Keys.USER_LOG_OUT;
}

export interface IHandleClear extends Action {
  readonly type: Keys.HANDLE_CLEAR;
  payload: {
    type: 'update' | 'postUserHistory';
  };
}

//#region Get Lessons IActions
export interface IGetLessons extends Action {
  readonly type: Keys.GET_LESSONS;
  payload: {
    page?: number;
    limit?: number;
    maxLevel: number;
  };
}

export interface IGetLessonsSuccess extends Action {
  readonly type: Keys.GET_LESSONS_SUCCESS;
  payload: ILessonRecord;
}

export interface IGetLessonsFail extends Action {
  readonly type: Keys.GET_LESSONS_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion

//#region Get Lesson By Id Actions
export interface IGetLessonById extends Action {
  readonly type: Keys.GET_LESSON_BY_ID;
  payload: {
    _id: string;
  };
}

export interface IGetLessonByIdSuccess extends Action {
  readonly type: Keys.GET_LESSON_BY_ID_SUCCESS;
  payload: any[];
}

export interface IGetLessonByIdFail extends Action {
  readonly type: Keys.GET_LESSON_BY_ID_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion

//#region Get User Info IActions
export interface IGetUserInfo extends Action {
  readonly type: Keys.GET_USER_INFO;
}

export interface IGetUserInfoSuccess extends Action {
  readonly type: Keys.GET_USER_INFO_SUCCESS;
  payload: IUserInfo;
}

export interface IGetUserInfoFail extends Action {
  readonly type: Keys.GET_USER_INFO_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion

//#region Update User Info IActions
export interface IUpdateUserInfo extends Action {
  readonly type: Keys.UPDATE_USER_INFO;
  payload: any;
}

export interface IUpdateUserInfoSuccess extends Action {
  readonly type: Keys.UPDATE_USER_INFO_SUCCESS;
  payload: any;
}

export interface IUpdateUserInfoFail extends Action {
  readonly type: Keys.UPDATE_USER_INFO_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion

//#region Get Vocabulary Packages IActions
export interface IGetVocabularyPackages extends Action {
  readonly type: Keys.GET_VOCABULARY_PACKAGES;
  payload: {
    root?: boolean;
    parent?: string;
  };
}

export interface IGetVocabularyPackagesSuccess extends Action {
  readonly type: Keys.GET_VOCABULARY_PACKAGES_SUCCESS;
  payload: any[];
}

export interface IGetVocabularyPackagesFail extends Action {
  readonly type: Keys.GET_VOCABULARY_PACKAGES_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion

//#region Get Vocabulary Actions
export interface IGetVocabulary extends Action {
  readonly type: Keys.GET_VOCABULARY;
  payload: {
    _id: string;
  };
}

export interface IGetVocabularySuccess extends Action {
  readonly type: Keys.GET_VOCABULARY_SUCCESS;
  payload: {data: IVocabularyPackage[]; pagination: IPagination};
}

export interface IGetVocabularyFail extends Action {
  readonly type: Keys.GET_VOCABULARY_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion

//#region Post User History Actions
export interface IPostUserHistory extends Action {
  readonly type: Keys.POST_USER_HISTORY;
  payload: {
    isAddGamesLevel: boolean;
    isAddVocabularyLevel: boolean;
    gameId: string;
    note?: string;
  };
}

export interface IPostUserHistorySuccess extends Action {
  readonly type: Keys.POST_USER_HISTORY_SUCCESS;
  payload: any;
}

export interface IPostUserHistoryFail extends Action {
  readonly type: Keys.POST_USER_HISTORY_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion

//#region Get User History IActions
export interface IGetUserHistory extends Action {
  readonly type: Keys.GET_USER_HISTORY;
  payload: {day: string; beginDate: string};
}

export interface IGetUserHistorySuccess extends Action {
  readonly type: Keys.GET_USER_HISTORY_SUCCESS;
  payload: IUserHistory[];
}

export interface IGetUserHistoryFail extends Action {
  readonly type: Keys.GET_USER_HISTORY_FAIL;
}
//#endregion

//#region Upload File Actions
export interface IUploadFiles extends Action {
  readonly type: Keys.UPDATE_FILES;
  payload: {
    file: ImagePickerResult;
  };
}

export interface IUploadFilesSuccess extends Action {
  readonly type: Keys.UPDATE_FILES_SUCCESS;
  payload: {url: string};
}

export interface IUploadFilesFail extends Action {
  readonly type: Keys.UPDATE_FILES_FAIL;
  payload: {
    errors: any;
  };
}
//#endregion
