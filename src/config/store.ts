import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../redux/reducers/rootReducer';
import {composeWithDevTools} from 'redux-devtools-extension';
import rootSaga from '../sagas/rootSaga';
import logger from 'redux-logger';

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware, logger];
const store = createStore(
	rootReducer,
	composeWithDevTools(applyMiddleware(...middleware)),
);
sagaMiddleware.run(rootSaga);

export default store;
