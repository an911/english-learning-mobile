/**
 * @file root sagas
 */

import {all} from 'redux-saga/effects';

// Place for sagas' app
import AppSaga from './appSaga';
import AuthenticationSaga from './authenticationSaga';

/*----Sagas List-----------------*/
export default function* rootSaga() {
	yield all([AppSaga(), AuthenticationSaga()]);
}
