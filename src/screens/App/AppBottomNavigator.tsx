import React from 'react';
import {
  BottomTabNavigationProp,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import {CompositeNavigationProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {AppRoutes, AppTabs} from '../../navigation';
import {LessonContainer, Profile, VocabularyContainer} from './components';
import {theme} from '../../components';
import {Feather} from '@expo/vector-icons';

const Tab = createBottomTabNavigator<AppTabs>();

export type AppBottomNavigationProps = CompositeNavigationProp<
  BottomTabNavigationProp<AppTabs>,
  StackNavigationProp<AppRoutes>
>;

const AppBottomNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Lesson"
      tabBarOptions={{
        tabStyle: {
          borderTopWidth: 1,
          borderTopColor: theme.colors.borderColorDefault,
        },
        activeTintColor: theme.colors.secondary,
        showLabel: false,
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          const icons: {[x: string]: string} = {
            Lesson: 'book',
            Profile: 'user',
            Vocabulary: 'grid'
          };
          return <Feather name={icons[route.name]} size={24} color={color} />;
        },
      })}>
      <Tab.Screen name="Lesson" component={LessonContainer} />
      <Tab.Screen name="Vocabulary" component={VocabularyContainer} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};

export default AppBottomNavigator;
