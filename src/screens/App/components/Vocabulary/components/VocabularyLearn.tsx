import React from 'react';
import {
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {theme, Text, ProcessBar, Box, Button} from '../../../../../components';
import CardFlip from 'react-native-card-flip';
import {AntDesign} from '@expo/vector-icons';
import {IVocabulary, IVocabularyPackage} from '../../../../../redux/model/IApp';
import {Feather} from '@expo/vector-icons';

export interface VocabularyProps {
  currentLesson: IVocabularyPackage;
  setChildScreen: () => void;
}

export const VocabularyLearn: React.FC<VocabularyProps> = ({
  currentLesson,
  setChildScreen,
}) => {
  const cardRef = React.useRef<CardFlip>(null);
  const vocabularyBank = currentLesson.vocabularies;
  const vocabularyLength = currentLesson.vocabularies.length;
  const [vocabularyIndex, setVocabularyIndex] = React.useState<number>(0);
  const [toggleResult, setToggleResult] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (currentLesson && vocabularyLength < 1) {
      Alert.alert('Thông báo', 'Bài học này hiện đang rỗng', [
        {
          text: 'Quay về',
          onPress: () => {
            setChildScreen();
          },
        },
      ]);
    }
    return () => {
      setToggleResult(false);
    };
  }, []);

  const onQuitLesson = () =>
    Alert.alert(
      'Thông báo',
      'Bạn có chắc chắn tiếp tục, kết quả bài học sẽ bị hủy!',
      [
        {
          text: 'Hủy',
        },
        {
          text: 'Tiếp tục',
          onPress: () => {
            setChildScreen();
          },
        },
      ],
    );

  const onCalcProcessWidth = () => {
    return (vocabularyIndex / vocabularyLength) * 100;
  };

  const renderCard = (vocabulary: IVocabulary) => {
    return (
      <Box flex={1} paddingHorizontal="m">
        <CardFlip style={{flex: 1, zIndex: 10}} ref={cardRef}>
          <TouchableOpacity
            style={[styles.card]}
            onPress={() => cardRef.current.flip()}>
            <Text variant="title">{vocabulary.english}</Text>
            <Text variant="label">{vocabulary.spell}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.card, {backgroundColor: theme.colors.secondary}]}
            onPress={() => cardRef.current.flip()}>
            <Text variant="title" color="white">
              {vocabulary.vietnamese}
            </Text>
            <Text variant="label" color="white">
              {vocabulary.example}
            </Text>
            <Text variant="label" color="white">
              {vocabulary.exampleTranslate}
            </Text>
          </TouchableOpacity>
        </CardFlip>
      </Box>
    );
  };

  const onNext = () => {
    const nextVocabularyIndex = vocabularyIndex + 1;
    if (nextVocabularyIndex < vocabularyLength) {
      setVocabularyIndex(nextVocabularyIndex);
    } else {
      setToggleResult(false);
    }
  };

  const onBack = () => {
    const prevVocabularyIndex = vocabularyIndex - 1;
    if (prevVocabularyIndex >= 0) {
      setVocabularyIndex(prevVocabularyIndex);
    }
  };

  if (toggleResult) {
    return (
      <SafeAreaView style={styles.container}>
        <Box flex={1} justifyContent="center" alignItems="center">
          <Text variant="title">Chúc mừng bạn đã hoàn thành bài học</Text>
        </Box>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Box
        flexDirection="row"
        paddingHorizontal="m"
        paddingTop="m"
        alignItems="center"
        justifyContent="space-between">
        <Box marginRight="s">
          <TouchableWithoutFeedback onPress={onQuitLesson}>
            <Feather name="x" size={40} color={theme.colors.alto} />
          </TouchableWithoutFeedback>
        </Box>
        <ProcessBar width={`${onCalcProcessWidth()}%`} />
      </Box>

      <Box paddingHorizontal="m">
        <Box flexDirection="row">
          <Box marginRight="s" justifyContent="center">
            <Text variant="label">Phát âm</Text>
          </Box>
          <Box style={styles.audio}>
            <AntDesign name="sound" size={24} color="white" />
          </Box>
        </Box>
      </Box>

      {renderCard(vocabularyBank[vocabularyIndex].vocabulary)}

      <Box
        paddingHorizontal="m"
        marginBottom="m"
        flexDirection="row"
        justifyContent="space-between">
        <Box flex={1 / 2} marginRight="s">
          <Button
            variant="secondary"
            disabled={vocabularyIndex < 1}
            onPress={onBack}
            label="Quay lại"
          />
        </Box>
        <Box flex={1 / 2}>
          <Button
            variant="primary"
            disabled={vocabularyIndex === vocabularyLength}
            onPress={onNext}
            label="Tiếp tục"
          />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  card: {
    flex: 1,
    borderRadius: 16,
    borderWidth: 2,
    marginVertical: theme.spacing.m,
    borderColor: theme.colors.borderColorDefault,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  audio: {
    width: 80,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    borderWidth: 2,
    borderColor: theme.colors.borderColorSecondary,
    borderBottomWidth: 4,
    backgroundColor: theme.colors.secondary,
  },
});
