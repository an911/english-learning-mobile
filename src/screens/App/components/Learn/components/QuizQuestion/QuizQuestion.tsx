import React from 'react';
import {Box, Text} from '../../../../../../components';
import {IAnswer, IQuestion} from '../../../../../../redux/model/IApp';
import {AnswerOption} from '../Option';
interface IQuizQuestionProps {
  question: IQuestion;
  selectedAnswer: IAnswer;
  onSelectAnswer: (data: IAnswer) => void;
}

const QuizQuestion: React.FC<IQuizQuestionProps> = ({
  question,
  selectedAnswer,
  onSelectAnswer,
}) => {
  const {answers} = question;

  return (
    <React.Fragment>
      <Box paddingHorizontal="m" marginBottom="m">
        <Text variant="title">{question.question}</Text>
      </Box>
      <Box
        flex={3}
        flexDirection="row"
        flexWrap="wrap"
        paddingHorizontal="m"
        justifyContent="space-around"
        alignItems="flex-end">
        {answers.map((item) => (
          <AnswerOption
            onSelectAnswer={() => onSelectAnswer(item)}
            selected={item._id === selectedAnswer?._id}
            key={item._id}
            item={item}
          />
        ))}
      </Box>
    </React.Fragment>
  );
};

export default QuizQuestion;
