import React from 'react';
import {View, Dimensions} from 'react-native';
import {Box, Text, theme} from '../../../../../../components';
import {LineChart} from 'react-native-chart-kit';
import {useDispatch, useSelector} from 'react-redux';
import {getUserHistory} from '../../../../../../redux/actions/app';
import IStore from '../../../../../../redux/model/store/IStore';

export default function ActivityChart() {
  const AppState = useSelector((state: IStore) => state.App);
  const dispatch = useDispatch();
  const [dataset, setDataset] = React.useState({
    labels: [],
    vocabularies: [],
    lessons: [],
  });

  const weekdays = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];

  const getDayName = (date: string) => {
    return weekdays[new Date(date).getDay()];
  };

  React.useEffect(() => {
    if (AppState.userHistoryRecords.length > 0) {
      const labels = [];
      const vocabularies = [];
      const lessons = [];

      for (const record of AppState.userHistoryRecords) {
        labels.push(getDayName(record.date));
        vocabularies.push(record.totalVocabularyPackages);
        lessons.push(record.totalGameLevel);
      }

      setDataset({
        labels,
        vocabularies,
        lessons,
      });
    }
  }, [AppState.userHistoryRecords]);

  React.useEffect(() => {
    const date = new Date();
    date.setDate(date.getDate() - 7);

    if (AppState.userHistoryRecords.length < 1) {
      dispatch(
        getUserHistory({
          day: '7',
          beginDate: date.getTime().toString(),
        }),
      );
    }
    // eslint-disable-next-line
  }, []);

  return (
    <Box marginBottom="m">
      <Text variant="body" marginBottom="s">
        Hoạt động
      </Text>
      {AppState.userHistoryRecords && (
        <LineChart
          data={{
            labels: dataset.labels,
            datasets: [
              {
                data: [1, 2, 3, 4],
              },
            ],
          }}
          width={Dimensions.get('window').width - theme.spacing.xl} // from react-native
          height={220}
          yAxisInterval={1} // optional, defaults to 1
          chartConfig={{
            backgroundColor: '#1cb0f6',
            backgroundGradientFrom: '#1cb0f6',
            backgroundGradientTo: '#1cb0f1',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16,
            },
            propsForDots: {
              r: '6',
              strokeWidth: '2',
              stroke: '#1cb0f6',
            },
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16,
          }}
        />
      )}
    </Box>
  );
}
