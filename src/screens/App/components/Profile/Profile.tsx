import React from 'react';
import {ScrollView, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, Text, theme} from '../../../../components';
import {AppRoutes, StackNavigationProps} from '../../../../navigation';
import {Feather} from '@expo/vector-icons';
import {Avatar, ActivityChart, UpdateInfoForm} from './components';
import IStore from '../../../../redux/model/store/IStore';
import {useDispatch, useSelector} from 'react-redux';
import {updateUserInfo} from '../../../../redux/actions/app';

export type ProfileNavigation = StackNavigationProps<AppRoutes, 'Home'>;
export interface ProfileProps extends ProfileNavigation {}

const Profile: React.FC<ProfileProps> = (props) => {
  const AppState = useSelector((state: IStore) => state.App);
  const dispatch = useDispatch();

  const {isUploadSuccessful, isGettingInfo, userInfo} = AppState;

  React.useEffect(() => {
    if (isUploadSuccessful) {
      updateUserInfo({
        avatar: userInfo.avatar,
      });
    }
  }, [isUploadSuccessful]);

  return (
    <SafeAreaView style={styles.container}>
      {isGettingInfo ? (
        <Box flex={1} justifyContent="center" alignContent="center">
          <Text variant="title" textAlign="center">
            Loading...
          </Text>
        </Box>
      ) : (
        <Box flex={1}>
          <Box
            position="relative"
            flexDirection="row"
            justifyContent="center"
            backgroundColor="white"
            borderBottomWidth={1}
            paddingTop="s"
            paddingBottom="m"
            borderBottomColor="borderColorDefault">
            <Text variant="title2">Hồ sơ</Text>

            {/* <Box
              position="absolute"
              top={0}
              right={0}
              height="100%"
              paddingTop="s"
              paddingRight="s">
              <TouchableWithoutFeedback>
                <Feather
                  name="settings"
                  size={32}
                  color={theme.colors.secondary}
                />
              </TouchableWithoutFeedback>
            </Box> */}
          </Box>
          <Box style={styles.profileHeader}>
            <Box>
              <Text variant="title" marginBottom="s">
                {userInfo && userInfo.name}
              </Text>
              <Box flexDirection="row">
                <Feather name="user" size={18} color={theme.colors.muted} />
                <Text variant="subTitle" marginLeft="xs">
                  Thông tin cá nhân của bạn
                </Text>
              </Box>
            </Box>
            {userInfo && <Avatar avatar={userInfo.avatar} />}
          </Box>

          {userInfo && (
            <ScrollView
              style={{
                flex: 1,
                paddingHorizontal: theme.spacing.m,
                backgroundColor: theme.colors.white,
              }}>
              <ActivityChart />
              <UpdateInfoForm {...props} />
            </ScrollView>
          )}
        </Box>
      )}
    </SafeAreaView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: theme.spacing.m,
    marginBottom: theme.spacing.m,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.borderColorDefault,
  },
});
