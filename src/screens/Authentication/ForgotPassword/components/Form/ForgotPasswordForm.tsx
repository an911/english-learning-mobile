import React from 'react';
import {Formik, FormikHelpers} from 'formik';
import {Alert} from 'react-native';
import * as yup from 'yup';
import {Box, Text, TextInput, Button, theme} from '../../../../../components';

const validationForgotPasswordSchema = yup.object().shape({
	email: yup
		.string()
		.email('Email không hợp lệ')
		.required('Email là trường bắt buộc'),
});

interface FormForgotPasswordValues {
	email: string;
}

const ForgotPasswordForm = () => {
	const initialValues = {
		email: '',
	};

	const onSubmit = (
		values: FormForgotPasswordValues,
		{setSubmitting}: FormikHelpers<FormForgotPasswordValues>,
	) => {
		console.log('values', values);
		// if (!isProcessing) {
		// 	Alert.alert(
		// 		'Thông báo',
		// 		'Quý khách có chắc chắn  thay đổi thông tin cá nhân',
		// 		[
		// 			{
		// 				text: 'Hủy',
		// 			},
		// 			{
		// 				text: ' Xác nhận',
		// 				onPress: () => {
		// 					if (toggleAddress) {
		// 						actions.updateUserInfo({
		// 							...updateInfo,
		// 							...homeInfo,
		// 						});
		// 					} else {
		// 						actions.updateUserInfo({
		// 							...updateInfo,
		// 							...companyInfo,
		// 						});
		// 					}
		// 				},
		// 			},
		// 		],
		// 	);
		// }
	};

	const showError = (errors: string, setErrors: () => void) => {
		Alert.alert('Thông báo', errors, [
			{
				text: 'Ok',
				onPress: setErrors,
			},
		]);
	};

	return (
		<React.Fragment>
			<Formik
				validateOnBlur={false}
				validateOnChange={true}
				initialValues={initialValues}
				onSubmit={onSubmit}
				validationSchema={validationForgotPasswordSchema}>
				{({handleChange, handleSubmit, errors, values, isValid}) => (
					<>
						<Box
							flex={1}
							justifyContent="space-between"
							alignItems="center">
							<Box width="100%">
								<TextInput
									value={values.email}
									placeholder="Email"
									onChangeText={handleChange('email')}
								/>
								{!isValid && (
									<Box marginBottom="m">
										<Text
											variant="body"
											color="persianRed"
											fontSize={15}
											letterSpacing={2}>
											{errors['email']}
										</Text>
									</Box>
								)}
							</Box>
							<Button
								variant="secondary"
								label="Gửi email"
								onPress={handleSubmit}
								disabled={!isValid || !values.email}
							/>
						</Box>
					</>
				)}
			</Formik>
		</React.Fragment>
	);
};

export default ForgotPasswordForm;
