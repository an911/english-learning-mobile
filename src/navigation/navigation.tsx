import {ParamListBase, RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

export type RootRoutes = {
  Authentication: undefined;
  App: undefined;
};

export type AuthenticationRoutes = {
  OnBoarding: undefined;
  GetStarted: undefined;
  Login: undefined;
  Register: undefined;
  ForgotPassword: undefined;
};

export type AppRoutes = {
  Home: undefined;
  ChangePassword: undefined;
  Learn: {
    lessonId: string;
  };
};

export type AppTabs = {
  Lesson: undefined;
  Profile: undefined;
  Vocabulary: undefined;
};

export interface StackNavigationProps<
  ParamList extends ParamListBase,
  RouteName extends keyof ParamList = string
> {
  navigation: StackNavigationProp<ParamList, RouteName>;
  route: RouteProp<ParamList, RouteName>;
}
